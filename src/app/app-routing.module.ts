import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PollComponent } from './poll/poll.component';

const routes: Routes = [
  { path: '', component: PollComponent },
  { path: 'home', loadChildren: './tabs/tabs.module#TabsPageModule' },
  /** Se puede cargar el modulo de esta forma o mediante el router
   *  que previamente cargamos con el modulo alojado en la ruta que
   *  se presenta anteriormente, en la primera ruta
   */
  { path: 'tab4', loadChildren: './tab4/tab4.module#Tab4PageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
