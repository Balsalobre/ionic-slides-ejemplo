import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  private data = [
    {
      category: 'Datos personales',
      preguntas: [
        { id: 0, input: '¿Qué edad tienes?' },
        { id: 1, input: 'Sexo' },
        { id: 2, input: 'Introduzca su peso' },
        { id: 3, input: 'Introduzca su altura' }
      ]
    },
    {
      category: 'Datos mock',
      preguntas: [
        { id: 0, input: '¿Ejemplo de pregunta?' },
        { id: 1, input: 'Ejemplo de input 1' },
        { id: 2, input: 'Ejemplo de input 2' },
        { id: 3, input: 'Ejemplo de input 3' }
      ]
    }
  ];

  constructor() { }

  getData() {
    return this.data;
  }
}
