import { Component, OnInit } from '@angular/core';
import { PollService } from '../poll.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss'],
})
export class PollComponent implements OnInit {

  slideOpts = {
    effect: 'flip'
  };

  poll = [];
  items = [];

  constructor(private pollService: PollService, private router: Router) { }

  ngOnInit() {
    this.items = this.pollService.getData();
    console.log(this.items);
  }
}